import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
	{
		path: "/",
		name: "Home",
		component: Home,
	},
	{
		path: "/find",
		name: "Find",
		component: () => import("../views/Find.vue"),
	},
	{
		path: "/aboutUs",
		name: "AboutUs",
		component: () => import("../views/AboutUs.vue"),
	},
	{
		path: "/plant/:id",
		name: "Plant",
		component: () => import("../views/Plant.vue"),
	},
	{
		path: "/about",
		name: "About",
		component: () => import("../views/About.vue"),
	},
	{
		path: "/auth",
		name: "Auth",
		component: () => import("../views/Auth.vue"),
	},
	{
		path: "/admin",
		name: "Admin",
		component: () => import("../views/Admin.vue"),
	},
];

const router = new VueRouter({
	routes,
	mode: "history",
});

export default router;
