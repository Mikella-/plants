import { client } from "./http-client";

export const getPlants = () => client.get("/plants").then(({ data }) => data);

export const getPlant = id =>
	client.get(`/plants/${id}`).then(({ data }) => data);

export const createPlant = plant =>
	client.post("/plants", plant).then(({ data }) => data);

export const updatePlant = plant =>
	client.put(`/plants/${plant._id}`, plant).then(({ data }) => data);

export const deletePlant = id =>
	client.delete(`/plants/${id}`).then(({ data }) => data);
