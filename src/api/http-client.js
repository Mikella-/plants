import { default as axios } from "axios";

const baseURL =
	"https://plants.mikella.art" || "http://localhost:3000" || location.origin;

export const client = axios.create({
	baseURL: `${baseURL}/api`,
	timeout: 50000,
	headers: {
		"Content-Type": "application/json",
	},
});

client.interceptors.request.use(config => {
	config.headers.Authorization = `${localStorage.getItem("token")}`;
	return config;
});
