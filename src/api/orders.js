import { client } from "./http-client";

export const getOrders = () => client.get("/orders").then(({ data }) => data);

export const createOrder = order =>
	client.post("/orders", order).then(({ data }) => data);

export const updateOrder = order =>
	client.put(`/orders/${order._id}`, order).then(({ data }) => data);

export const markOrderAsPaid = id =>
	client.put(`/orders/${id}/mark-as-paid`).then(({ data }) => data);
