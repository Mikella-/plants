import { client } from "./http-client";

export const getComments = plant =>
	client.get(`/comments/${plant}`).then(({ data }) => data);

export const getCommentsForModeration = () =>
	client.get(`/comments`).then(({ data }) => data);

export const createComment = comment =>
	client.post("/comments", comment).then(({ data }) => data);

export const removeComment = id =>
	client.delete(`/comments/${id}`).then(({ data }) => data);
