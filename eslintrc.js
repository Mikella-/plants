module.exports = {
	root: true,
	env: {
		node: true,
	},
	extends: ["plugin:vue/recommended", "eslint:recommended", "@vue/prettier"],
	parserOptions: {
		parser: "babel-eslint",
	},
	rules: {
		"no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
		"no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
		"no-unused-vars": process.env.NODE_ENV === "production" ? "error" : "warn",
		"vue/no-unused-components":
			process.env.NODE_ENV === "production" ? "error" : "warn",
		"vue/component-name-in-template-casing": [
			"error",
			"kebab-case",
			{
				registeredComponentsOnly: true,
			},
		],
		"vue/valid-v-slot": [
			"error",
			{
				allowModifiers: true,
			},
		],
		"prettier/prettier": [
			"warn",
			{
				singleQuote: true,
				semi: false,
				trailingComma: "all",
				arrowParens: "avoid",
				htmlWhitespaceSensitivity: "ignore",
			},
		],
	},
};
